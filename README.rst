.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl
   :alt: License: AGPL-3


================================
Partner add Postal Address
================================

Adds a new type of address : Postal address
This module has been pushed to oca in partner-contact on PR#763 <https://github.com/OCA/partner-contact/pull/763>

Credits
=======

Contributors
------------

* Rémi Cazenave <remi@le-filament.com>


Maintainer
----------

.. image:: https://le-filament.com/images/logo-lefilament.png
   :alt: Le Filament
   :target: https://le-filament.com

This module is maintained by Le Filament
